<?php require_once "./code.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S02 Activity</title>
</head>
<body>
	<h1>Divisibles of 5</h1>
	
	<p><?php divisible(); ?></p>

	<h1>Array Manipulation</h1>

	<?php $students = [];

	array_push($students, "John Smith"); ?>

	<p><?php var_dump($students); ?></p>
	<p><?php echo count($students); ?> </p>

	<?php array_push($students, "Jane Smith"); ?>

	<p><?php var_dump($students); ?></p>
	<p><?php echo count($students); ?> </p>

	<?php array_shift($students); ?>
	<p><?php var_dump($students); ?></p>
	<p><?php echo count($students); ?> </p>

	
</body>
</html>