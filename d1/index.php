<?php require_once "./code.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<h1>Repitition Control structures</h1>
	<h3>While Loop</h3>
	<p><?php echo whileLoop(); ?></p>
	<h3>Do While Loop</h3>
	<p><?php echo doWhileLoop(); ?></p>
</body>
</html>