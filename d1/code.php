<?php

/*
Repition control structures
*/

function whileLoop(){
	$count = 0;

	while($count <= 5){
		echo $count . '</br>';
		$count++;
	}
}


function doWhileLoop(){
	$count = 20;

	do{
		echo $count . '</br>';
		$count--;
	}while($count > 0);
}